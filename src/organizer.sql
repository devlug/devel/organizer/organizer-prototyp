-- ===========================================
-- DDL für organizer
-- ===========================================

-- == Konfiguration
-- Diese Tabelle speichert die Konfiguration der
-- Anwendungen. Aufgeteilt in Kontext, Gruppe und
-- Schlüssel. 

CREATE TABLE IF NOT EXISTS Config (
	CONTEXT VARCHAR(50),
	GROUPNAME VARCHAR(50),
	KEY VARCHAR(50),
	VALUE VARCHAR(50),
	PRIMARY KEY (CONTEXT, GROUPNAME, KEY)
);

-- Tabelle für die Ländercodes nach ISO 3166
-- https://de.wikipedia.org/wiki/ISO-3166-1-Kodierliste

CREATE TABLE IF NOT EXISTS ISO3166 (
	ID INTEGER PRIMARY KEY,
	NAME VARCHAR(50),
	ALPHA2 VARCHAR(2),
	ALPHA3 VARCHAR(3)
);

-- Tabelle für Bundesländern
-- Name und ein Foreign Key auf das Land via Tabelle ISO3166.

CREATE TABLE IF NOT EXISTS State (
	ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	NAME VARCHAR(50),
	COUNTRY_FK INTEGER,
	FOREIGN KEY(COUNTRY_FK) REFERENCES ISO3166(ID)
);

-- Tabelle für Städe
-- Postleitzahl, Name der Stadt, Foreign Key auf das Bundesland

CREATE TABLE IF NOT EXISTS City (
	ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	ZIP_CODE VARCHAR(10),
	NAME VARCHAR(100),
	STATE_FK INTEGER, 
	FOREIGN KEY(STATE_FK) REFERENCES State(ID)
);

-- Tabelle für Adressen
-- Straßennamen, Hausnummer und Foreign Key auf die Stadt

CREATE TABLE IF NOT EXISTS Address (
	ID VARCHAR(50) PRIMARY KEY,
	STREET VARCHAR(50),
	HOUSE_NUMBER VARCHAR(5),
	CITY_FK INTEGER,
	FOREIGN KEY(CITY_FK) REFERENCES City(ID)
);

-- Tabelle für Kontakte

CREATE TABLE IF NOT EXISTS Contact (
	ID VARCHAR(50) PRIMARY KEY,
	FIRSTNAME VARCHAR(40),
	LASTNAME VARCHAR(40),
	EMAIL_PERSONAL VARCHAR(200),
	EMAIL_WORK VARCHAR(200),
	BDAY_D INTEGER,
	BDAY_M INTEGER,
	BDAY_Y INTEGER
);

-- Tabelle für Aufgaben

CREATE TABLE IF NOT EXISTS Task (
	ID VARCHAR(50) PRIMARY KEY,
	NAME VARCHAR(100),
	DUEDATE REAL
);

-- ========================================================
-- Daten
-- ========================================================

-- Konfiguration - global.defaults.country auf 276 (DE)
INSERT INTO Config(CONTEXT, GROUPNAME, KEY, VALUE) VALUES ("global","defaults","country","276");

-- Einträge für die Ländercodes 
INSERT INTO ISO3166(ID,NAME,ALPHA2,ALPHA3) VALUES( 250, "France", "FR", "FRA");
INSERT INTO ISO3166(ID,NAME,ALPHA2,ALPHA3) VALUES( 276, "Germany", "DE", "DEU");

-- Einträge für Bundesländern
INSERT INTO State(NAME, COUNTRY_FK) VALUES("Hessen", 276);

