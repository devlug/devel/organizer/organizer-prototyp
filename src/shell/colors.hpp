/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <string>

namespace organizer
{

/**
 * @brief Terminal Farben
 *
 * In diesem Namespace sind Klassen für die Verwendung
 * von Farben in Terminal Anwendungen.
 *
 * @author Stefan Kropp
 * @date 2018-05-10
 *
 */
namespace colors
{

/**
 * @brief Enumeration Default Color Codes.
 *
 * In diesem enum sind die Standard Farbcodes definiert.
 *
 * @author Stefan Kropp
 * @date 2018-05-10
 */
enum class Color {

    /**
     * @brief Black
     */
    Black = 0,

    /**
    * @brief Red
    */
    Red = 1,

    /**
    * @brief Green
    */
    Green = 2,

    /**
     @bbreif Yellow
    */
    Yellow = 3,

    /**
    * @brief Blue
    */
    Blue = 4,

    /**
    * @brief Magenta
    */
    Magenta = 5,

    /**
    * @brief Cyan
    */
    Cyan = 6,

    /**
    * @bbrief White
    */
    White = 7,

    /**
    * @brief Bright Black
    */
    BrightBlack = 8,

    /**
    * @brief Bright Red
    */
    BrightRed = 9,

    /**
    * @brief Bright Green
    */
    BrightGreen = 10,

    /**
    * @brief Bright Yellow
    */
    BrightYellow = 11,

    /**
    * @brief Bright Blue
    */
    BrightBlue = 12,

    /**
    * @brief Bright Magenta
    */
    BrightMagenta = 13,

    /**
    * @brief Bright Cyan
    */
    BrightCyan = 14,

    /**
    * @brief Bright White
    */
    BrightWhite = 15
};

/**
 * @brief Enumeration Attribute für die Anzeige
 *
 * In diesem enum sind die Attribute für die Anzeige definiert.
 *
 * @author Stefan Kropp
 * @date 2018-05-10
 */

enum class Attributes {

    /**
    * @brief Reset
    *
    */
    Reset = 0,

    /**
    * @brief  Bright
    */
    Bright = 1,

    /**
    * @brief dim
    */
    Dim = 2,

    /**
    * @brief underline
    */
    Underline = 4,

    /**
    * @brief blink
    */
    Blink = 5,

    /**
    * @brief reverse
    */
    Reverse = 7
};

/**
 * @brief Colors for Terminal
 *
 * Diese Klasse ist zur Verwendung von Farben im Terminal.
 * Terminal Ausgaben in Farben erfolgen über ANSI ESCAPE Codes.
 * Informationen gibt es in man 4 console_codes und auf Wikipedia:
 * https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
 *
 * @author Stefan Kropp
 * @version 0.1
 *
 */
class Colors
{
  public:
    Colors();

    /**
     * @brief gets the ANSI ESCAPE Code
     *
     * @param attribute The attribute
     * @param foreground Foreground Color Code
     * @param background Background Color Code
     *
     * @returns ANSI ESCAPE Color Code
    */
    std::string getColor(Attributes attribute, Color foreground,
                         Color background);
    std::string get256Foreground(int n);
    std::string get256Background(int n);

    /**
     * @brief gets colored text
     *
     */

    std::string getText(Attributes attribute, Color foreground,
                        Color background, std::string text);

    std::string sysinfo();

  private:
    Attributes defaultAttributes;
    Color defaultForeground;
    Color defaultBackground;
};
}
}
