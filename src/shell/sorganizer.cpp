/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sorganizer.hpp"
#include "../core/organizer.hpp"
#include "../dto/date.hpp"
#include "colors.hpp"
#include <iostream>
#include <string>
#include <sys/ioctl.h>

using namespace organizer;

int main(int argc, char *argv[])
{
    SOrganizer sorganizer;
    sorganizer.init();
    sorganizer.run();
    return 0;
}

SOrganizer::SOrganizer() : Base("SOrganizer")
{
}

void SOrganizer::init()
{
    logger->logDebug("SOrganizer::init");
    organizer = new Organizer();
    organizer->init();
}

void SOrganizer::run()
{

    for (;;) {
        std::string command;
        std::cout << "sorganizer> " << std::flush;
        getline(std::cin, command);
        exec(command);
    }
}

void SOrganizer::exec(std::string cmd)
{

    colors::Colors c;

    logger->logDebug(cmd.c_str());
    if (cmd.compare("hello") == 0) {
        std::cout << "Hello! How are you?" << std::endl;
        organizer::entity::Date d;
        std::cout << "Heute ist " << d.to_string() << std::endl;
    } else if (cmd.compare("help") == 0) {
        std::cout << "help - Hilfe" << std::endl;
        std::cout << "hello - Today" << std::endl;
        std::cout << "ls - Zeige Kontkate" << std::endl;
        std::cout << "search <text> - Kontkate suchen" << std::endl;
        std::cout << "new - Neuer Kontkat" << std::endl;
    } else if (cmd.compare("ls") == 0) {
        std::list<organizer::entity::Contact> list = organizer->getContacts();
        std::cout << "Kontkate wurden gelesen: " << list.size() << std::endl;
        display(list);
    } else if (cmd.substr(0, 6).compare("search") == 0) {
        std::string query = cmd.substr(7);
        std::list<organizer::entity::Contact> list =
            organizer->findContacts(query);
        display(list);
    } else if (cmd.compare("new") == 0) {
        newContact();
    } else if (cmd.compare("sysinfo") == 0) {
        struct winsize size;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);

        std::cout << "lines: " << size.ws_row;
        std::cout << " columns: " << size.ws_col << std::endl;
        std::cout << c.sysinfo() << std::endl;

    } else {
        std::cout << "What? Do you need help? Enter help." << std::endl;
    }
}

/**
 * @brief Anzeiger einer Liste von Kontakten.
 *
 * Diese Methode schreibt direkt in std::cout.
 *
 */

void SOrganizer::display(std::list<organizer::entity::Contact> cl)
{
    colors::Colors c;
    bool ct = false;
    for (std::list<organizer::entity::Contact>::iterator it = cl.begin();
         it != cl.end(); it++) {

        // Baut die Zeile als String
        std::string line;
        line.append(it->getLastName());
        line.append(", ");
        line.append(it->getFirstName());
        line.append(",");
        line.append(it->getEMailPersonal());
        line.append(", ");
        line.append(it->getEMailWork());
        if (it->getBirthday() != NULL) {
            line.append(", ");
            line.append(it->getBirthday()->to_string());
        }

        // Color Toggle
        if (ct) {
            std::cout << c.getText(colors::Attributes::Reset,
                                   colors::Color::BrightWhite,
                                   colors::Color::Black, line);
        } else {
            std::cout << c.getText(colors::Attributes::Reset,
                                   colors::Color::BrightWhite,
                                   colors::Color::BrightBlack, line);
        }
        std::cout << std::endl;
        ct = !ct;
    }
}

void SOrganizer::newContact()
{
    std::string nachname;
    std::cout << "Nachname: ";
    getline(std::cin, nachname);

    std::string vorname;
    std::cout << "Vorname: ";
    getline(std::cin, vorname);

    std::string mailPrivat;
    std::cout << "E-Mail (privat): ";
    getline(std::cin, mailPrivat);

    std::string mailWork;
    std::cout << "E-Mail (beruflich): ";
    getline(std::cin, mailWork);

    std::cout << nachname << ", " << vorname << std::endl;

    this->logger->logDebug("Neue Person %s, %s", nachname, vorname);

    entity::Contact *c = new entity::Contact();
    c->setFirstName(vorname);
    c->setLastName(nachname);
    c->setEMailPersonal(mailPrivat);
    c->setEMailWork(mailWork);

    organizer->createNewContact(c);
}
