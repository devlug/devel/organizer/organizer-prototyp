/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "colors.hpp"

using namespace organizer::colors;

Colors::Colors()
{
    defaultAttributes = Attributes::Reset;
    defaultForeground = Color::White;
    defaultBackground = Color::Black;
}

std::string Colors::getColor(Attributes attribute, Color foreground,
                             Color background)
{
    std::string color;
    // attribut
    color.append("\033[");
    color.append(std::to_string((int)attribute));
    color.append("m");

    // foreground
    color.append("\033[");
    color.append("38:5:");
    color.append(std::to_string((int)foreground));
    color.append("m");

    // background
    color.append("\033[");
    color.append("48:5:");
    color.append(std::to_string((int)background));
    color.append("m");

    return color;
}

std::string Colors::get256Foreground(int n)
{
    std::string color;
    return color;
}
//"\033[48:5:165m\033[38:5:219mTest"

std::string Colors::get256Background(int n)
{
    std::string color;
    color.append("\033[48:5:");
    color.append(std::to_string(n));
    color.append("m");
    return color;
}

std::string Colors::getText(Attributes attribute, Color foreground,
                            Color background, std::string text)
{
    std::string result;
    result.append(getColor(attribute, foreground, background));
    result.append(text);
    result.append(
        getColor(defaultAttributes, defaultForeground, defaultBackground));
    return result;
}

std::string Colors::sysinfo()
{
    std::string result;
    result.append(getText(defaultAttributes, defaultForeground,
                          defaultBackground, "Default\t"));
    result.append(getText(defaultAttributes, Color::Black, Color::White,
                          "Black / White\t"));
    result.append(
        getText(defaultAttributes, Color::Red, Color::Green, "Red / Green\t"));
    result.append(getText(defaultAttributes, Color::Yellow, Color::Blue,
                          "Yellow / Blue\t"));
    result.append(getText(defaultAttributes, Color::Magenta, Color::Cyan,
                          "Magenta / Cyan\t"));
    result.append(getText(defaultAttributes, Color::BrightBlack,
                          Color::BrightWhite, "BrightBlack / BrightWhite\t"));
    result.append(getText(defaultAttributes, Color::BrightRed,
                          Color::BrightGreen, "BrightRed / BrightGreen\t"));
    result.append(getText(Attributes::Underline, Color::BrightRed,
                          Color::BrightGreen,
                          "Underline / BrightRed / BrightGreen\t"));
    return result;
}
