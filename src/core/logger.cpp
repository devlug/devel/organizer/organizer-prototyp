/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "logger.hpp"
#include <fstream>
#include <iostream>
#include <stdarg.h>

using namespace organizer::utils;

Logger::Logger()
{
}

Logger::Logger(std::string name)
{
    this->name = name;
}

void Logger::logFatalError(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("FATAL", fmt, args);
}

void Logger::logError(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("ERROR", fmt, args);
}

void Logger::logWarning(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("WARNING", fmt, args);
}

void Logger::logMessage(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("MESSAGE", fmt, args);
}

void Logger::logVerbose(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("VERBOSE", fmt, args);
}

void Logger::logStatus(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("STATUS", fmt, args);
}

void Logger::logSysError(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("SYSERR", fmt, args);
}

void Logger::logDebug(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("DEBUG", fmt, args);
}

void Logger::logTrace(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    log("TRACE", fmt, args);
}

void Logger::log(std::string level, const char *fmt, va_list args)
{
    std::ofstream file;
    file.open("organizer.debug.log", std::ios::app);
    char message[1024];
    std::vsprintf(message, fmt, args);
    file << "[" << level << "][" << this->name << "] " << message << std::endl;
}
