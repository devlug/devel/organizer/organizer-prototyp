/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "organizer.hpp"
#include "../dao/dao.hpp"

using namespace organizer;

Organizer::Organizer() : Base("Organizer")
{
}

void Organizer::init()
{
    this->logger->logFatalError("Test Log");
}

void Organizer::createNewContact(entity::Contact *contact)
{
    database::ContactDAO *dao = new database::ContactDAO();
    dao->create(contact);
}

std::list<entity::Contact> Organizer::getContacts()
{
    logger->logDebug("getContacts\n");
    std::list<entity::Contact> contacts;

    database::ContactDAO dao = database::ContactDAO();
    contacts                 = dao.getAll();
    logger->logDebug("getContacts done\n");
    return contacts;
}

std::list<entity::Contact> Organizer::findContacts(std::string query)
{
    logger->logDebug("findContacts: \n");
    std::list<entity::Contact> contacts = getContacts();
    std::list<entity::Contact> result;

    for (std::list<organizer::entity::Contact>::iterator it = contacts.begin();
         it != contacts.end(); it++) {
        bool found = false;
        if (it->getLastName().find(query) != std::string::npos) {
            found = true;
        }
        if (found) {
            result.push_back(*it);
        }
    }

    return result;
}

