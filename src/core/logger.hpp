/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __LOGGER_H_
#define __LOGGER_H_

#include <string>

namespace organizer::utils
{

/**
 * @brief 	Logger
 *			Der Logger schreibt Informationen in die Datei
 *organizer.debug.log.
 *			Bei der Entwicklung oder beim Testen kann die Datei bei
 *Verwendung
 *			Der Anwendung via tail -f organizer.debug.log eingesehen
 *werden.
 *
 *			Beispiel: [DEBUG][CmdLineParsing] Parsing Cmd Line
 *arguments...
 *
 * @date So 3. Dez 13:12:47 CET 2017
 * @author Stefan Kropp
 * @version 0.1
 */

class Logger
{
  public:
    Logger(std::string name);

    void logFatalError(const char *fmt, ...);

    void logError(const char *fmt, ...);

    void logWarning(const char *fmt, ...);

    void logMessage(const char *fmt, ...);

    void logVerbose(const char *fmt, ...);

    void logStatus(const char *fmt, ...);

    void logSysError(const char *fmt, ...);

    void logDebug(const char *fmt, ...);

    void logTrace(const char *fmt, ...);

  private:
    Logger();
    void log(std::string level, const char *fmt, va_list args);
    std::string name;
};
}

#endif
