/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __ORGANIZER_H_
#define __ORGANIZER_H_

#include "../dto/contact.hpp"
#include "base.hpp"
#include <list>

namespace organizer
{

/**
 * @brief Organizer
 * @date So 3. Dez 13:06:36 CET 2017
 * @author Stefan Kropp
 * @version 0.1
 */
class Organizer : public Base
{
  public:
    Organizer();
    void init();
    std::list<entity::Contact> getContacts();
    std::list<entity::Contact> findContacts(std::string query);
    void createNewContact(entity::Contact *contact);

  private:
};
}

#endif
