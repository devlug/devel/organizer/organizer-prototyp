/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "utils.hpp"
#include <iostream>
#include <sys/time.h>
#include <unistd.h>

using namespace organizer::utils;

Utils::Utils()
{
}

/*
 * Ermittelt den hostname.
 * @return String mit dem Hostname
 */

std::string Utils::getHostname()
{
    char hostname[1024];
    if (gethostname(hostname, sizeof(hostname))) {
        exit(1);
    }
    return hostname;
}

int Utils::getRandomNumber()
{
    int r = rand();
    return r;
}

std::string Utils::getTimestamp()
{
    std::string result = "";
    timeval tv;
    int error = gettimeofday(&tv, NULL);
    if (error == 0) {
        result = std::to_string(tv.tv_sec);
        result += std::to_string(tv.tv_usec);
    }
    return result;
}
