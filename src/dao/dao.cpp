/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "dao.hpp"
#include "../dto/date.hpp"
#include <string.h>
#include <string>
using namespace organizer;

#define CONTACT_ID "ID"
#define CONTACT_FIRSTNAME "FIRSTNAME"
#define CONTACT_LASTNAME "LASTNAME"
#define CONTACT_EMAIL_PERSONAL "EMAIL_PERSONAL"
#define CONTACT_EMAIL_WORK "EMAIL_WORK"
#define CONTACT_BDAY_D "BDAY_D"
#define CONTACT_BDAY_M "BDAY_M"
#define CONTACT_BDAY_Y "BDAY_Y"

void map(entity::Contact *contact, char *colName, char *value);
static int callbackContact(void *contact, int argc, char **argv,
                           char **azColName);

void map(entity::Contact *contact, char *colName, char *value)
{
    if (strcmp(CONTACT_ID, colName) == 0) {
        // TODO: Parsen der ID
    } else if (strcmp(CONTACT_FIRSTNAME, colName) == 0) {
        contact->setFirstName(std::string(value));
    } else if (strcmp(CONTACT_LASTNAME, colName) == 0) {
        contact->setLastName(std::string(value));
    } else if (strcmp(CONTACT_EMAIL_PERSONAL, colName) == 0) {
        contact->setEMailPersonal(std::string(value));
    } else if (strcmp(CONTACT_EMAIL_WORK, colName) == 0) {
        contact->setEMailWork(std::string(value));
    } else if (strcmp(CONTACT_BDAY_D, colName) == 0 && value != NULL) {
        entity::BDay *bday = contact->getBirthday();
        if (bday == NULL) {
            bday = new entity::BDay();
            contact->setBirthday(bday);
		}
        bday->setDay(std::atoi(value));
    } else if (strcmp(CONTACT_BDAY_M, colName) == 0 && value != NULL) {
        entity::BDay *bday = contact->getBirthday();
        bday->setMonth(std::atoi(value));
    } else if (strcmp(CONTACT_BDAY_Y, colName) == 0 && value != NULL) {
        entity::BDay *bday = contact->getBirthday();
        bday->setYear(std::atoi(value));
    }
}


static int callbackContact(void *contact, int argc, char **argv,
                           char **azColName)
{
    int i;
    for (i = 0; i < argc; i++) {
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    return 0;
}


static int callbackContactList(void *contactList, int argc, char **argv,
                               char **azColName)
{
    std::list<entity::Contact> *list =
        (std::list<entity::Contact> *)contactList;
    entity::Contact *contact = new entity::Contact();
    int i;
    for (i = 0; i < argc; i++) {
        map(contact, azColName[i], argv[i]);
    }
    list->push_back(*contact);
    return 0;
}


database::ContactDAO::ContactDAO() : Base("ContactDAO")
{

    char *zErrMsg = 0;
    int rc;
    logger->logDebug("Open sqlite3 db...\n");
    rc = sqlite3_open("organizer.db", &db);
    if (rc) {
        fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
        sqlite3_close(db);
    } else {
    	logger->logDebug("Open sqlite3 db done: %s\n", sqlite3_errmsg(db));
	}
}

/**
ID VARCHAR(20) PRIMARY KEY,
FIRSTNAME VARCHAR(40),
LASTNAME VARCHAR(40),
EMAIL_PERSONAL VARCHAR(200),
EMAIL_WORK VARCHAR(200)
*/
void database::ContactDAO::create(entity::Contact *contact)
{
    logger->logDebug("crate\n");

    char *zErrMsg = 0;
    int rc;

    std::string values = std::string();
    values.append("INSERT INTO "
                  "Contact(ID,FIRSTNAME,LASTNAME,EMAIL_PERSONAL,EMAIL_WORK)  "
                  "VALUES(");
    values.append("\"");
    values.append(contact->getId().getString().c_str());
    values.append("\",");
    values.append("\"");
    values.append(contact->getFirstName().c_str());
    values.append("\",");
    values.append("\"");
    values.append(contact->getLastName().c_str());
    values.append("\",");
    values.append("\"");
    values.append(contact->getEMailPersonal().c_str());
    values.append("\",");
    values.append("\"");
    values.append(contact->getEMailWork().c_str());
    values.append("\"");
    values.append(")");

    logger->logDebug(values.c_str());

    rc = sqlite3_exec(db, values.c_str(), NULL, NULL, &zErrMsg);
    sqlite3_int64 rowid = sqlite3_last_insert_rowid(db);
    logger->logMessage("sqlite3_last_insert_rowid %d", rowid);

    if (rc == SQLITE_OK) {
        logger->logError(sqlite3_errmsg(db));
        sqlite3_free(zErrMsg);
    } else {
        logger->logMessage("Contact crated.");
    }
}
void database::ContactDAO::update(entity::Contact *contact)
{
}

void database::ContactDAO::remove(entity::Contact *contact)
{

    char *zErrMsg = 0;
    int rc;

    // DELETE FROM Contact WHERE ID = "<ID>";
    std::string values = std::string();
    values.append("DELETE FROM Contact");
    values.append(" WHERE ID = ");
    values.append("\"");
    values.append(contact->getId().getString().c_str());
    values.append("\"");

    logger->logDebug(values.c_str());

    rc = sqlite3_exec(db, values.c_str(), NULL, NULL, &zErrMsg);

    if (rc) {
        logger->logError(sqlite3_errmsg(db));
    } else {
        logger->logMessage("Contact deleted.");
    }
}

entity::Contact *database::ContactDAO::get(std::string id)
{
    entity::Contact *contact = NULL;
    char *zErrMsg            = 0;
    int rc;
    std::string queryString = std::string();
    queryString.append("SELECT * FROM Contact");
    queryString.append(" WHERE ID = ");
    queryString.append("\"");
    queryString.append(id.c_str());
    queryString.append("\"");

    rc = sqlite3_exec(db, queryString.c_str(), callbackContact, contact,
                      &zErrMsg);

    if (rc) {
        logger->logError(sqlite3_errmsg(db));
    } else {
        logger->logMessage("Contact read");
    }
    return NULL;
}

std::list<entity::Contact> database::ContactDAO::getAll()
{
    std::list<entity::Contact> contacts = std::list<entity::Contact>();

    char *zErrMsg = 0;
    int rc;
    std::string queryString = std::string();
    queryString.append("SELECT * FROM Contact");
    logger->logDebug("DB Query: %s\n", queryString.c_str());
    rc = sqlite3_exec(db, queryString.c_str(), callbackContactList, &contacts,
                      &zErrMsg);
    logger->logDebug("Done!");
    if (rc) {
        logger->logError("Error: %s", sqlite3_errmsg(db));
    } else {
        logger->logMessage("Contact read");
    }

    return contacts;
}
