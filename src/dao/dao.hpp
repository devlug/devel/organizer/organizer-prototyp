/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __CONTACT_DAO_HPP_
#define __CONTACT_DAO_HPP_

#include "../core/base.hpp"
#include "../dto/contact.hpp"
#include <list>
#include <sqlite3.h>

namespace organizer
{
namespace database
{

class ContactDAO : public Base
{
  public:
    ContactDAO();
    void create(entity::Contact *contact);
    void update(entity::Contact *contact);
    void remove(entity::Contact *contact);
    entity::Contact *get(std::string id);
    std::list<entity::Contact> getAll();

  private:
    sqlite3 *db;
};

}
}

#endif
