/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ADDRESS_HPP
#define ADDRESS_HPP

namespace organizer
{
namespace entity
{
/**
 * @brief 	Entity Address.
 *			Data Object "Adresse"
 *
 * @date So 3. Dez 12:30:15 CET 2017
 * @author Stefan Kropp
 * @version 0.1
 */
class Address
{
  public:
    Address();
};
}   // entity
}   // organizer

#endif
