/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "id.hpp"
#include "../utils/utils.hpp"
#include <iostream>
using namespace organizer::entity;

/**
 * Erzeugt ein ID-Objekt.
 *
 * Die ID soll eindeutig sein, auch über mehrere Rechner.
 * Um die ID eindeutig zu setzen, werden die folgenden Daten verwendet.
 *
 * @param hostname Hostname wo die ID erzeugt wurde
 * @param timestamp Zeitstempl wann die ID erzeugt wurde
 * @param rndNumber Zufällige Zahl
 *
 */
Id::Id(std::string hostname, std::string timestamp, std::string rndNumber)
{
    this->name      = hostname;
    this->timestamp = timestamp;
    this->rnd       = rndNumber;
}

/**
 * Erstellen einer ID.
 *
 * Mit dieser Methode kann eine ID erstellt werden.
 * @return Eine ID.
 */

Id *Id::generate()
{
    std::string hostname  = organizer::utils::Utils::getHostname();
    std::string timestamp = organizer::utils::Utils::getTimestamp();
    std::string randomNumber =
        std::to_string(organizer::utils::Utils::getRandomNumber());
    Id *generatedId = new Id(hostname, timestamp, randomNumber);
    return generatedId;
}

std::string Id::getString()
{
    return this->name + "-" + this->timestamp + "-" + this->rnd;
}

Id *parse(std::string)
{
    return new Id("Dummy", "Fixme", "ToDo");
}
