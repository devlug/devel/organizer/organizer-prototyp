/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __CONTACT_H_
#define __CONTACT_H_

#include "date.hpp"
#include "id.hpp"
#include <string>

namespace organizer
{
namespace entity
{
/**
 * @brief Entity Contact
 * @date So 3. Dez 12:28:10 CET 2017
 * @version 0.1
 * @author Stefan Kropp
 **/
class Contact
{
  public:
    Contact();
    Contact(std::string id);
    Id getId();
    void setId(Id *);
    void setFirstName(std::string);
    void setLastName(std::string);
    void setEMailPersonal(std::string);
    void setEMailWork(std::string);
    void setBirthday(BDay *date);
    std::string getFirstName();
    std::string getLastName();
    std::string getEMailPersonal();
    std::string getEMailWork();
    BDay *getBirthday();

  private:
    Id *id;
    std::string firstname;
    std::string lastname;
    std::string eMailPersonal;
    std::string eMailWork;
    BDay *birthday;
};
}
}
#endif
