/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "date.hpp"
#include <time.h>

using namespace organizer::entity;

Date::Date()
{
    d = boost::gregorian::date(boost::gregorian::day_clock::local_day());
}

Date::Date(std::string date)
{
}

std::string Date::to_string()
{
    boost::gregorian::date d1(boost::gregorian::day_clock::local_day());
    std::string result;
    result.append(boost::gregorian::to_iso_extended_string(d1));
    return result;
}

//============================================================================
// BDay
//============================================================================

BDay::BDay()
{
    this->d = 0;
    this->m = 0;
    this->y = 0;
}

BDay::BDay(short d, short m)
{
    this->d = d;
    this->m = m;
    this->y = 0;
}

BDay::BDay(short d, short m, short y)
{
    this->d = d;
    this->m = m;
    this->y = y;
}

std::string BDay::to_string()
{
    std::string bday;
    bday.append(std::to_string(d));
    bday.append(".");
    bday.append(std::to_string(m));
    bday.append(".");
    bday.append(std::to_string(y));
    return bday;
}

short BDay::getDay()
{
    return d;
}

short BDay::getMonth()
{
    return m;
}

short BDay::getYear()
{
    return y;
}

void BDay::setDay(short d)
{
    this->d = d;
}

void BDay::setMonth(short m)
{
    this->m = m;
}

void BDay::setYear(short y)
{
    this->y = y;
}
