/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "contact.hpp"

using namespace organizer::entity;

Contact::Contact()
{
    id = Id::generate();
	birthday = NULL;
}

Id Contact::getId()
{
    return *id;
}

void Contact::setId(Id *id)
{
    this->id = id;
}

void Contact::setFirstName(std::string name)
{
    firstname = name;
}

void Contact::setLastName(std::string name)
{
    lastname = name;
}
std::string Contact::getFirstName()
{
    return firstname;
}

std::string Contact::getLastName()
{
    return lastname;
}

BDay *Contact::getBirthday()
{
    return birthday;
}

void Contact::setEMailPersonal(std::string email)
{
    eMailPersonal = email;
}

void Contact::setEMailWork(std::string email)
{
    eMailWork = email;
}

void Contact::setBirthday(BDay *date)
{
    birthday = date;
}

std::string Contact::getEMailPersonal()
{
    return eMailPersonal;
}

std::string Contact::getEMailWork()
{
    return eMailWork;
}
