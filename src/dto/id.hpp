/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#ifndef __ID_H_
#define __ID_H_

namespace organizer
{
namespace entity
{

/**
 * Klasse für eine technische ID.
 * @author Stefan Kropp
 *
 **/
class Id
{
  public:
    Id(std::string, std::string, std::string);
    static Id *generate();
    static Id *parse(std::string);
    std::string getString();

  private:
    std::string name;
    std::string timestamp;
    std::string rnd;
};
}
}

#endif
