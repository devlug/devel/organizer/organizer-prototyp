/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __DATE_H_
#define __DATE_H_

#include <ctime>
#include <string>

#include "boost/date_time/gregorian/gregorian.hpp"

namespace organizer
{
namespace entity
{
/**
 * @brief 	Enity Date
 *			Ein Datum - intern via boost::gregorian::date.
 *
 * @date So 3. Dez 12:32:40 CET 2017
 * @author Stefan Kropp
 * @version 0.1
 */
class Date
{
  public:
    /*!
     * @brief 	Current date
     *		 	By default the current date (today)
     */
    Date();

    /*!
     * @brief	Date defined in parameter date.
     *
     * @param	date	date as string
     */
    Date(std::string date);

    /*!
     * @brief	Date to string
     *
     * @return	Date as string
     */
    std::string to_string();

  private:
    boost::gregorian::date d;
};

/**
 * @brief Geburtstag
 *
 * Diese Klasse definiert ein Geburtstag. Ein Geburtstag kann nur Tag und Monat
 * sein,
 * wenn z.b. das Geburtsjahr nicht bekannt ist oder Tag, Monat und Jahr.
 *
 * @date Di 6. Mär 08:25:37 CET 2018
 * @author Stefan Kropp
 * @version 0.1
 */

class BDay
{
  public:
    static const short UNKOWN = 0;

    BDay();

    /**
     * @param d Tag
     * @param m Monat
     */
    BDay(short d, short m);

    /**
     * @param d Tag
     * @param m Monat
         * @param y Jahr
     */
    BDay(short d, short m, short y);

    short getDay();
    short getMonth();
    short getYear();

    void setDay(short);
    void setMonth(short);
    void setYear(short);

    std::string to_string();

  private:
    short d;
    short m;
    short y;
};
}
}

#endif
