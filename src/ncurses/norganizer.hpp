/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __NORGANIZER_H_
#define __NORGANIZER_H_

#include "../core/base.hpp"
#include "../core/organizer.hpp"
#include <menu.h>
#include <panel.h>

namespace organizer
{

namespace ncurses
{

/**
* @brief Entity Contact
* @date 2018-22-20
* @version 0.1
* @author Stefan Kropp
*/
class NOPanel : public Base
{
  public:
    NOPanel();
    void ShowPanel();
    void HidePanel();

  protected:
    PANEL *panel;
    WINDOW *window;
};

/**
* @brief Entity Contact
* @date 2018-22-20
* @version 0.1
* @author Stefan Kropp
*/
class NOMenu : public NOPanel
{
  public:
    NOMenu();
    int handlePressKey(int key);
    bool isTodaySelected();
    bool isContactSelected();

  private:
    MENU *menu;
    ITEM *today;
    ITEM *contact;
    ITEM *task;
};

/**
* @brief Entity Contact
* @date 2018-22-20
* @version 0.1
* @author Stefan Kropp
*/
class NOStatus : public NOPanel
{
  public:
    NOStatus();
    void printstatus(const char *fmt, va_list);
};

class NOToday : public NOPanel
{
  public:
    NOToday();
};

class NOContactList : public NOPanel
{
  public:
    NOContactList();
    void setList(std::list<organizer::entity::Contact>);
    void showContactList();
    int handlePressKey(int key);
    std::list<organizer::entity::Contact> list;

  private:
    void scrollDown();
    void scrollUp();
    int selected;
};


class NOrganizer : public Base
{
  public:
    NOrganizer();
    void init();
    void run();
    void printstatus(const char *fmt, ...);
    void showContactList();

  private:
    organizer::Organizer *organizer;
    PANEL *createHelpPanel();
    NOMenu *menu;
    NOStatus *status;
    NOToday *today;
    NOContactList *contactList;
    int handlePressKey(int c);
    void hideAllPanels();
};

}   // ncurses
}   // organizer

#endif
