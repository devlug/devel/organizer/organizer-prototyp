/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "norganizer.hpp"
#include <curses.h>
#include <form.h>
#include <panel.h>
#include <signal.h>

using namespace organizer::ncurses;

#define COLOR_DEFAULT 1
#define COLOR_MENU 2
#define COLOR_STATUS 3
#define COLOR_DATA 4
#define COLOR_SUBWIN 5

static void finish(int sig);

int main(int argc, char *argv[])
{
    NOrganizer client = NOrganizer();
    client.run();
    return 0;
}

//==============================================================================
// NOrganizer
//==============================================================================
NOrganizer::NOrganizer() : Base("NOrganizer")
{
    init();
}

void NOrganizer::init()
{
    logger->logDebug("NOrganizer init...");
    signal(SIGINT, finish);
    initscr();
    keypad(stdscr, TRUE);
    nonl();
    cbreak();
    noecho();
    curs_set(0);
    if (has_colors()) {
        start_color();
        // COLOR_BLACK, COLOR_WHITE, COLOR_RED, COLOR_GREEN,
        // COLOR_YELLOW, COLOR_BLUE, COLOR_CYAN, COLOR_MAGENTA
        init_pair(COLOR_DEFAULT, COLOR_WHITE, COLOR_BLACK);
        init_pair(COLOR_MENU, COLOR_WHITE, COLOR_BLUE);
        init_pair(COLOR_STATUS, COLOR_RED, COLOR_BLUE);
        init_pair(COLOR_DATA, COLOR_BLACK, COLOR_YELLOW);
        init_pair(COLOR_SUBWIN, COLOR_BLACK, COLOR_MAGENTA);
    }
    this->organizer   = new Organizer();
    this->menu        = new NOMenu();
    this->status      = new NOStatus();
    this->contactList = new NOContactList();
    this->today       = new NOToday();
}

void NOrganizer::run()
{
    logger->logDebug("Starting NOrganizer");
    this->menu->ShowPanel();
    this->status->ShowPanel();
    this->contactList->ShowPanel();
    this->today->ShowPanel();

    std::list<organizer::entity::Contact> list = organizer->getContacts();
    this->contactList->setList(list);
    showContactList();


    for (;;) {
        update_panels();
        refresh();
        int cmd = 0;
        int c   = getch();
        cmd     = handlePressKey(c);
        refresh();
        update_panels();
        doupdate();
    }
}

void NOrganizer::printstatus(const char *fmt, ...)
{
    va_list args;
    va_start(args, fmt);
    this->status->printstatus(fmt, args);
}

void NOrganizer::showContactList()
{
    this->contactList->showContactList();
}

PANEL *NOrganizer::createHelpPanel()
{
    const int MARGIN = 3;
    WINDOW *win = newwin(LINES - MARGIN * 2, COLS - MARGIN * 2, MARGIN, MARGIN);
    werase(win);
    box(win, 0, 0);
    wattrset(win, COLOR_PAIR(COLOR_SUBWIN));
    wbkgd(win, COLOR_PAIR(COLOR_SUBWIN));
    mvwprintw(win, 1, 1, "help");
    mvwprintw(win, 2, 1, "h\tHilfe");
    mvwprintw(win, 3, 1, "s\tShow contact");
    wrefresh(win);
    PANEL *p = new_panel(win);
    return p;
};

void NOrganizer::hideAllPanels()
{
    this->contactList->HidePanel();
    this->today->HidePanel();
}

int NOrganizer::handlePressKey(int c)
{
    printstatus("Key %X", c);
    int cmd = 0;
    if (c == KEY_RIGHT || c == KEY_LEFT) {
        int code = this->menu->handlePressKey(c);
        if (code != E_OK) {
            printstatus("menu_driver failed!");
        }
    } else if (c == KEY_DOWN || c == KEY_UP) {
        if (menu->isContactSelected()) {
            contactList->handlePressKey(c);
        }
    }
    hideAllPanels();
    if (menu->isTodaySelected()) {
        this->today->ShowPanel();
    } else if (menu->isContactSelected()) {
        this->contactList->ShowPanel();
    }
    update_panels();
    refresh();

    if (c == KEY_F(1)) {
    }
}
//==============================================================================
// NOPanel
//==============================================================================

NOPanel::NOPanel()
{
}

void NOPanel::ShowPanel()
{
    show_panel(panel);
    wrefresh(window);
}

void NOPanel::HidePanel()
{
    hide_panel(panel);
}

//==============================================================================
// NOToday
//==============================================================================
NOToday::NOToday()
{
    const int MARGIN = 3;
    window           = newwin(LINES - MARGIN * 2, COLS - MARGIN * 2, 3, 3);
    box(window, ACS_VLINE, ACS_HLINE);
    wbkgd(window, COLOR_PAIR(COLOR_DEFAULT));
    panel = new_panel(window);
    mvwprintw(window, 1, 1, "*** Keine Einträge ***");
}

//==============================================================================
// NOMenu
//==============================================================================

NOMenu::NOMenu()
{
    const int HEIGHT = 1;
    window           = newwin(HEIGHT, COLS, 0, 0);
    wbkgd(window, COLOR_PAIR(COLOR_MENU));
    today   = new_item("Heute", "");
    contact = new_item("Kontakte", "");
    task    = new_item("Aufgaben", "");

    ITEM *items[] = {today,
                     contact,
                     task,
                     new_item("Notizen", ""),
                     new_item("Termine", ""),
                     new_item("Kalender", ""),
                     0};
    menu = new_menu(items);
    set_menu_format(menu, 1, 5);
    box(stdscr, ACS_VLINE, ACS_HLINE);
    set_menu_win(menu, window);
    post_menu(menu);
    set_menu_fore(menu, COLOR_PAIR(COLOR_MENU) | A_REVERSE);
    set_menu_back(menu, COLOR_PAIR(COLOR_MENU));
    panel = new_panel(window);
}

int NOMenu::handlePressKey(int key)
{
    int cmd = 0;
    if (key == KEY_RIGHT) {
        cmd = REQ_RIGHT_ITEM;
    }
    if (key == KEY_LEFT) {
        cmd = REQ_LEFT_ITEM;
    }
    int code = menu_driver(menu, cmd);
    return code;
}

bool NOMenu::isTodaySelected()
{
    return current_item(menu) == today;
}

bool NOMenu::isContactSelected()
{
    return current_item(menu) == contact;
}

//==============================================================================
// NOStatus
//==============================================================================

NOStatus::NOStatus()
{
    const int HEIGHT = 2;
    window           = newwin(HEIGHT, COLS, LINES - HEIGHT, 0);
    wbkgd(window, COLOR_PAIR(COLOR_STATUS));
    // wattrset(window, COLOR_PAIR(COLOR_STATUS));
    mvwprintw(window, 1, 1, "norganizer");
    panel = new_panel(window);
}

void NOStatus::printstatus(const char *fmt, va_list args)
{
    attrset(COLOR_PAIR(COLOR_STATUS));
    mvwprintw(window, 1, 1, "norganizer");
    wmove(window, 1, 15);
    vwprintw(window, fmt, args);
    wrefresh(window);
    attrset(COLOR_PAIR(COLOR_DEFAULT));
}

//==============================================================================
// NOContactList
//==============================================================================

NOContactList::NOContactList()
{
    selected         = 0;
    const int MARGIN = 3;
    // WINDOW* win = subwin(stdscr, LINES-MARGIN*2, COLS-MARGIN*2, MARGIN ,
    // MARGIN);
    window = newwin(LINES - MARGIN * 2, COLS - MARGIN * 2, MARGIN, MARGIN);
    scrollok(window, TRUE);
    werase(window);
    box(window, 0, 0);
    wattrset(window, COLOR_PAIR(COLOR_DEFAULT));
    wbkgd(window, COLOR_PAIR(COLOR_DEFAULT));
    mvwprintw(window, 1, 1, "Kontaktdaten");
    wrefresh(window);
    panel = new_panel(window);
}

int NOContactList::handlePressKey(int key)
{
    if (key == KEY_DOWN || key == KEY_UP) {
        if (key == KEY_DOWN) {
            selected++;
        } else if (key == KEY_UP) {
            selected--;
        }
    }
    showContactList();
    return 0;
}

void NOContactList::scrollDown()
{
    showContactList();
    wrefresh(window);
}

void NOContactList::scrollUp()
{
    showContactList();
    wrefresh(window);
}

void NOContactList::setList(std::list<organizer::entity::Contact> list)
{
    this->list = list;
}

void NOContactList::showContactList()
{
    int linesPerPage = 20;
    int x            = list.size();
    int displayId    = selected / linesPerPage;
    werase(window);
    wbkgd(window, COLOR_PAIR(COLOR_DATA));
    mvwprintw(window, 1, 1, "%d Kontakte (Seite %d)", x, displayId + 1);
    int i = 2;
    int c = 0;

    for (std::list<organizer::entity::Contact>::iterator it = list.begin();
         it != list.end(); it++) {
        if (c >= displayId * linesPerPage) {
            std::string contact;
            if (c == selected) {
                wattrset(window, COLOR_PAIR(COLOR_STATUS));
                contact.append("*");
            } else {
                wattrset(window, COLOR_PAIR(COLOR_DATA));
                contact.append(" ");
            }

            contact.append(std::to_string(c + 1));
            contact.append("\t");
            contact.append(it->getLastName().substr(0, 30));
            contact.append("\t");
            contact.append(it->getFirstName().substr(0, 30));
            contact.append("\t");
            contact.append(it->getEMailPersonal().substr(0, 30));
            contact.append("\t");
            contact.append(it->getBirthday()->to_string());

            mvwprintw(window, i++, 1, contact.c_str());
        }
        c++;
    }
    wrefresh(window);
}


static void finish(int sig)
{
    endwin();
    exit(0);
}
