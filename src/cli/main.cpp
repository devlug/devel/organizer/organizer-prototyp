/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "main.hpp"
#include "cmdLineParsing.hpp"
#include "../dto/contact.hpp"
#include "../dto/id.hpp"
#include "../core/organizer.hpp"
#include "../utils/utils.hpp"
#include <iostream>

using namespace organizer::cli;

MainCli::MainCli()
{
}

void MainCli::run()
{
}

int main(int argc, char *argv[])
{

    organizer::CmdLineParsing cmdLineParsing =
        organizer::CmdLineParsing(argc, argv);

    /* Version-Flag gesetzt, anzeige der Version */
    if (cmdLineParsing.isFlagVersion()) {
        std::cout << "Organizer Version 0.0.1-DEV" << std::endl;
        std::cout << "Virtuelle Linux User Group Hessen" << std::endl;
        std::cout << "Stefan Kropp" << std::endl;
        std::cout << "http://www.vlug-hessen.de/project/organizer.html"
                  << std::endl;
        exit(0);
    }

    /* Hilfe-Flag gesetzt, anzeige der Version */
    if (cmdLineParsing.isFlagHelp()) {
        std::cout << "organizer [<options>]" << std::endl << std::endl;
        std::cout << "Optionen:" << std::endl;
        std::cout << "-l --list              " << '\t' << "Liste alle Kontakte"
                  << std::endl;
        std::cout << "-m --mutt-query <query>" << '\t' << "mutt / neomutt query"
                  << std::endl;
        exit(0);
    }

    /* List-Flag wurde gesetzt, alle Kontakte anzeigen */
    if (cmdLineParsing.isFlagList()) {
        organizer::Organizer *organizer = new organizer::Organizer();
        organizer->init();
        std::cout << "Lese alle Kontakte" << std::endl;
        std::list<organizer::entity::Contact> list = organizer->getContacts();
        std::cout << "Kontkate wurden gelesen: " << list.size() << std::endl;
        for (std::list<organizer::entity::Contact>::iterator it = list.begin();
             it != list.end(); it++) {
            std::cout << it->getLastName() << ", " << it->getFirstName() << ", "
                      << it->getEMailPersonal() << ", " << it->getEMailWork()
                      << ", " << it->getBirthday()->to_string() << std::endl;
        }
        exit(0);
    } else if (cmdLineParsing.isFlagMutt()) {
        organizer::Organizer *organizer = new organizer::Organizer();
        organizer->init();
        std::cout << std::endl;   // Mutt braucht ein new line als erstes
        std::string query = cmdLineParsing.getMuttQuery();
        std::list<organizer::entity::Contact> list =
            organizer->findContacts(query);
        for (std::list<organizer::entity::Contact>::iterator it = list.begin();
             it != list.end(); it++) {
            std::cout << it->getEMailPersonal() << "\t" << it->getFirstName()
                      << " " << it->getLastName() << std::endl;
        }
    }

    MainCli *cli = new MainCli();
    cli->run();
    return 0;
}
