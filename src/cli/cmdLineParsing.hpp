/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef __CMD_LINE_PASING_H_
#define __CMD_LINE_PASING_H_

#include "../core/base.hpp"

namespace organizer
{
class CmdLineParsing : public Base
{
  public:
    CmdLineParsing(int argc, char **argv);
    bool isFlagHelp();
    bool isFlagVersion();
    bool isFlagList();
    bool isFlagBirthday();
    bool isFlagMutt();
    std::string getMuttQuery();

  private:
    int verbose_flag;
    bool help_flag;
    bool version_flag;
    bool list;
    bool birthdayFlag;
    bool muttFlag;
    std::string muttQuery;
};
}

#endif
