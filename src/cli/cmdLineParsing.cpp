/* This file is part of organizer.
 * Copyright © 2018 Stefan Kropp <stefan.kropp@vlug-hessen.de>
 *
 * organizer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * organizer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with organizer. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "cmdLineParsing.hpp"
#include <getopt.h>

using namespace organizer;

CmdLineParsing::CmdLineParsing(int argc, char **argv) : Base("CmdLineParsing")
{
    verbose_flag = false;
    help_flag    = false;
    version_flag = false;
    list         = false;
    birthdayFlag = false;
    muttFlag     = false;

    this->logger->logDebug("Parsing Cmd Line arguments...");
    int c;
    while (1) {
        static struct option long_options[] = {
            /* These options set a flag. */
            {"verbose", no_argument, &verbose_flag, 1},
            {"brief", no_argument, &verbose_flag, 0},
            /* These options don’t set a flag.
               We distinguish them by their indices. */
            {"help", no_argument, NULL, 'h'},
            {"version", no_argument, NULL, 'v'},
            {"list", no_argument, NULL, 'l'},
            {"birthday", no_argument, NULL, 'b'},
            {"mutt-query", required_argument, NULL, 'm'},
            {0, 0, 0, 0}};
        int option_index = 0;
        c = getopt_long(argc, argv, "lm:hvb", long_options, &option_index);
        if (c == -1) {
            break;
        } else {
            switch (c) {
            case 0:
                if (long_options[option_index].flag != 0)
                    break;
                printf("option %s", long_options[option_index].name);
                if (optarg)
                    printf(" with arg %s", optarg);
                printf("\n");
                break;
            case 'l':
                list = true;
                puts("option -l\n");
                break;
            case 'm':
                muttFlag = true;
                if (optarg) {
                    muttQuery.append(optarg);
                }
                break;
            case 'v':
                version_flag = true;
                break;
            case 'h':
                help_flag = true;
                break;
            case 'b':
                birthdayFlag = true;
                break;
            default:
                abort();
            }
        }
    }

    this->logger->logDebug("Parsing... done!");

    std::string verbose_flag_info = "verbose_flag: ";
    verbose_flag_info.append(std::to_string(verbose_flag));
    this->logger->logDebug(verbose_flag_info.c_str());

    std::string list_info = "list: ";
    list_info.append(std::to_string(list));
    this->logger->logDebug(list_info.c_str());
}

bool CmdLineParsing::isFlagVersion()
{
    return version_flag;
}

bool CmdLineParsing::isFlagHelp()
{
    return help_flag;
}

bool CmdLineParsing::isFlagList()
{
    return list;
}

bool CmdLineParsing::isFlagMutt()
{
    return muttFlag;
}

std::string CmdLineParsing::getMuttQuery()
{
    return muttQuery;
}
